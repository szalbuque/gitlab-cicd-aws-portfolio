# CI/CD using GitLab & deploy over an AWS EC2 instance

## Objective
Use GitLab CI/CD tools to publish and update a website in a webserver every time the repository receives a push.

## Resources
* GitHub repository: https://github.com/szalbuque/portfolio
* AWS Linux EC2 instance with GitLab Runner and Apache docker container
* GitLab repository

## Steps
1. Create and prepare EC2 instance
2. Configure GitLab
3. Test

## Create and prepare EC2 instance
### Requirements:
* Update and upgrade Linux
* Install Docker
* Install and configure GitLab Runner
* Create Inbound rule to open ports 80 and 22

## Configure GitLab
* Create a new project: https://gitlab.com/szalbuque/gitlab-cicd-aws-portfolio
* Connect with GitHub repository
  * Create a personal access token in GitHub in Settings > Developer Settings > Personal access tokens > Tokens (classic) > Generate new token > fine-graned
    * More information at: https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token
    * Give permission over the desired repository
    * Give read-only permission on Commit statuses, Contents and Metadata
* Configure GitLab Runner

## Test
* Open the address http://IPDOWEBSERVER in a browser, to view the website content;
* Open the source code in VSCode and make a change;
* Upload the change to the GitHub repository (commit and push);
* Open GitLab to verify the CI/CD jobs running;
* Refresh the browser to verify the changes made.

